var bg = chrome.extension.getBackgroundPage();

var btnPlay = document.getElementById("playControl");

bg.getSongsJSON();

btnPlay.addEventListener("click", function() {
    if (bg.isPaused()) {
        bg.play();
        btnPlay.className = "pause";
    }
    else {
        bg.pause();
        btnPlay.className = "play";
    }
})

document.getElementById("songsPlaying").innerHTML = bg.getSongs();


