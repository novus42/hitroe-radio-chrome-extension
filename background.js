var player = document.getElementById("hitroe");
var xhr = new XMLHttpRequest();
var songs;
var songsPlaying = document.getElementById("songsPlaying");

setInterval(getSongsJSON, 15000);

function play() {
    player.src = "http://hitroe.com:8000/stream_original";
    player.play();
}

function pause() {
    player.pause();
    player.src = "";
}

function getSongs() {
    return songsPlaying.innerHTML;
}

function isPaused() {
    return !!player.paused;
}

function updateSongsList(songs) {
    songsPlaying.innerHTML = "";
    for (var i=0; i<songs.length; i++) {
        var format = document.createElement("div");
        if (i == 0) format.className = "current";
        format.innerHTML = songs[i]["artist"] + " - " + songs[i]["title"];
        songsPlaying.appendChild(format);
    }

}
function getSongsJSON() {
    xhr.open("GET", "http://www.hitroe.com/api/songs/last/2324343434/", true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            songs = JSON.parse(xhr.responseText);
        }
    }
    xhr.send();
    if (songs) {
        updateSongsList(songs);
    } else {
        console.log("no songs");
    }
}